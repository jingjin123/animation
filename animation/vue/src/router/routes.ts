import Home from '@/views/Home.vue'

export const routes = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/home',
        component: Home,
        children: [
            {
                path: 'shake',
                name: 'shake',
                component: () => import('@/views/Shake.vue')
            },
            {
                path: 'loading',
                name: 'loading',
                component: () => import('@/views/Loading.vue')
            },
            {
                path: 'hover',
                name: 'hover',
                component: () => import('@/views/Hover.vue')
            },
            {
                path: 'transition',
                name: 'transition',
                component: () => import('@/views/Transition.vue')
            },
            {
                path: 'cool',
                name: 'cool',
                component: () => import('@/views/Cool.vue')
            }
        ]
    }
]